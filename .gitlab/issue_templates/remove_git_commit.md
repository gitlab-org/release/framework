## Context

> **WARNING**: Because of the destructive nature of rewriting git history, removing commits from Security and Dev repositories is limited to Delivery team members.

*Describe the situation why the git commit removal is necessary.*

References:

*URLs to the incident's issue, security issue, and other related issues and merge requests*

-
-

## Acknowledgment

- [ ] This procedure is NOT done on the GitLab Rails repository (`gitlab-org/gitlab`) and its mirrors
- [ ] I am a team member of `Delivery:Release` or `Delivery:Deploy` team
- [ ] I have read and am following the runbook [Remove Security Commit From A GitLab Component](https://gitlab.com/gitlab-org/release/docs/-/blob/master/runbooks/security/remove-security-commit.md)

## Data

- Change technician: {+TODO+} `Name(s) or GitLab handler(s) of the engineer(s) performing the procedure`
- Repository:
  - Security: {+TODO+} `URL to SECURITY mirror of the component`
  - Dev: {+TODO+} `URL to DEV mirror of the component`
- Security merge request with the commits to remove: {+TODO+} `MR`
- Commits to remove: {+TODO+} `List of commits to remove`
- Target commit: {+TODO+} `The last git commit before the security commits we want to remove`
  - [ ] Confirm there is no commit after the security commits that we want to remove.
  - [ ] Check git history and pick the last commit before the to-be-removed security commits.

## Before start

> **WARNING**: This procedure only applies for removing commits at the HEAD of a branch. If it is required to
> remove in-between commits, then this procedure needs to adapt accordingly.
 
- [ ] Fill in all [Data](#data)
- [ ] Provide all data and get approvals from:
  - [ ] Senior Engineering Manager of Delivery and/or VP of Engineering, Infrastructure.
  - [ ] AppSec team
  - [ ] Stage group owners (Optional. If it impedes the release schedule, a notification on Slack is sufficient) 


- [ ] (Optional but encouraged): The following steps are not a MUST but they are recommended to have a safe
  procedure.
  - [ ] Make backups of both `security` and `dev` mirrors:

    ```bash
    cd <backup-directory>
    mkdir security && git clone --no-single-branch <SECURITY mirror> security
    cd ..
    mkdir dev && git clone --no-single-branch <DEV mirror> dev
    ```

  - [ ] To make sure we have a clean git state, perform the following steps in a new directory. Do not reuse the existing
  clones on engineer's local machine.

## Start a group call

During the git history rewriting, it is better to do in pair to have another pair of eyes to validate the steps and
boost the confidence of the engineer.

- [ ] Start a Zoom call with another engineer, either from Software Delivery group or the service owner team.

## Erase the commit from GitLab Security

### Prepare `security`

Modify the protected branches settings in `security` {+TODO+} `URL to SECURITY mirror of the component`

- [ ] Add your username to `allowed to push and merge` on the default branch
- [ ] Enable `Allowed to force push` on the default branch

### Script to run on `security`

- [ ] Clone the security mirror

```git
git clone <SECURITY mirror>
```

- [ ] Checkout the default branch

```git
git checkout -b <default-branch>
```

- [ ] Check the commit history to be sure it is similar to remote (i.e. the last commits are similar)
- [ ] Reset the branch back to the target commit:

```bash
git reset --hard <target-commit>
```

- [ ] Confirm the last commit is the latest commit
  - Run `git log` and verify the last commit is the target commit
- [ ] Push to the security remote

```git
git push <default-branch> <security-remote> --force
```

- [ ] If the backport MRs were merged already, perform the above steps again with the stable branches

### Clean up `security`

Modify the `Security` protected branches settings {+TODO+} `URL to SECURITY mirror of the component`

- [ ] Remove your username to `allowed to push and merge`
- [ ] Disable `Allowed to force push`

## Erase the commit from GitLab Dev

### Prepare `dev`

Modify the protected branches settings ind `dev` {+TODO+} `URL to DEV mirror of the component`

- [ ] Enable `Allowed to force push` for maintainers on the default and stable branches

### Script to run on `dev`

- [ ] Add the dev remote

```git
git remote add dev <DEV mirror>
```

- [ ] Confirm the remotes

```git
git remote -v
```

Output:

```text
dev     <DEV mirror> (fetch)
dev     <DEV mirror> (push)
origin  <SECURITY mirror> (fetch)
origin  <SECURITY mirror> (push)
```

- [ ] Push to the `dev` remote

```git
git push --force dev
```

### Clean up `dev`

Modify the `Dev` protected branches settings {+TODO+} `URL to DEV mirror of the component`

- [ ] Disable `Allowed to force push` from the default branch

## Finalize

- [ ] Retrigger the mirroring configurations `canonical -> security` and `security -> dev` to make sure now the mirroring works again
- [ ] Verify Security and Dev are properly mirrored by triggering the `/chatops run mirror status` command

<!-- don't edit below -->
---
:notepad_spiral: This template can be edited [here](https://gitlab.com/gitlab-com/gl-infra/delivery/-/blob/main/.gitlab/issue_templates/remove_git_commit.md) :notepad_spiral:

/confidential
/label ~Delivery::P2 ~group::delivery ~workflow-infra::Triage