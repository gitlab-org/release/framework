Token management should follow the [company guidelines for automation and access
tokens](https://about.gitlab.com/handbook/engineering/automation/).

:warning: **Don't reuse an existing token** :warning:

Never reuse an existing token for a new automation, as this makes it harder to track what a token is used for. It also increases the number of changes required if the token is revoked.

If a token is already available in the project is ok to use it for a new feature (without storing it into a new variable), but every project on each instance, should have its own tokens.

:warning: **Use project access tokens by default** :warning:

If possible for your automation, make use of a [project access token](https://docs.gitlab.com/ee//user/project/settings/project_access_tokens). A perfect example for a project access token is [security mirroring](https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/security/mirrors.md).

## Where to store the token

If the token is used by automation and fetched directly from the Vault, store the token in Vault in the proper location. Otherwise, store the token in 1Password under `Releases` vault and add `Delivery/AccessToken` tag.

## Token details

The following fields describe where we generate the token

- token type: {+TODO+} `project, group, personal (associated with a bot), trigger, deploy`
- name: {+TODO+}
- owner: {+TODO+} `a bot user, a project or a group`
- instance: {+TODO+} `OPS, DEV, .COM pick one - this is the instance were we generate the token`
- scope: {+TODO+} `api, read_api, read_user, read_repository, write_repository, read_registry, write_registry`
- role: {+TODO+} `guest, reporter, developer, maintainer, owner - NA for personal tokens`
- expiry date: {+TODO+} `1 year by default, specify the date`
- generation date {+TBD by the provisioner+}

### Token usage

The following fields describe where we store the token.

- software: {+TODO+} `a link to the project using the token`
- instance: {+TODO+} `OPS, DEV, .COM pick one - this is the instance were we utilize the token`
- used by: {+TODO+} `a variable name with link to the repository or link to the code where it's fetched from the Vault`
- vault: {+TODO by the provisioner+} `if token is fetched from the Vault, put the link to the Vault entry, NaN in other cases`
- 1password: {+TODO by the provisioner+} `if 1Password is used, view the secret, click on three dots and select Copy Private Link. Post it here`

{+ describe the token usage +}


# Provisioning tasks

## Author

- [ ] complete the template with the details
- [ ] link any related issues to the work requiring this new token
- [ ] assign the issue to the token provisioner, it can be yourself

## Provisioner

- [ ] generate the new token
- [ ] install the token in the intended location
- [ ] update the generation date above in this issue
- [ ] set Due Date for the issue to token expiration date
- [ ] store the token in 1Password or Vault, depending on the usage. Add `Delivery/AccessToken` tag if 1Password was used
- [ ] update the links to 1Password or Vault above in this issue
- [ ] close this issue

# Token rotation

Token rotation workflow:

- reopen this issue
- leave a comment describing why we are rotating the token
- uncheck all the tasks in the provisioner list
- run the provisioner tasks

<!-- don't edit below -->
---
:notepad_spiral: This template can be edited [here](https://gitlab.com/gitlab-com/gl-infra/delivery/-/blob/main/.gitlab/issue_templates/access_token_request.md) :notepad_spiral: 

/confidential
/label ~"access-token"
